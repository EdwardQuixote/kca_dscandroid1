package ke.co.chaward.KCA_DSC.KCA_DSCAndroid1;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LinearLayout lilayMainLayout = (LinearLayout) this.findViewById(R.id.lilayMain);

        final EditText edUserEmail = (EditText) this.findViewById(R.id.edEmailAddress);

        final TextInputLayout tilUserPassword = (TextInputLayout) this.findViewById(R.id.tilPassword);
        final TextInputEditText tiedUserPassword = (TextInputEditText) this.findViewById(R.id.tiedPassword);

        Button btnSignIn = (Button) this.findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String sUserEmail = edUserEmail.getText().toString();
                if (sUserEmail.equals("")) {

                    Snackbar.make(lilayMainLayout, "Email" + sUserEmail, Snackbar.LENGTH_LONG).show();

                } else if (TextUtils.isEmpty(tiedUserPassword.getText())) {

                    tilUserPassword.setError("Please provide Password!");

                }

            }

        });

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
